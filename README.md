# 파일 구성
1. backup_alarm.sh (백업 스크립트)
2. restore_alarm.sh (밀어넣기 스크립트)

# 스크립트 실행 방법
## 백업 대상 PM 접속


### 1. 스크립트 다운로드 및 압축해제
```
  tar xvzf alarm_backup.tar.gz
```

### 2. 백업 스크립트 실행
```
  ./backup_alarm.sh
```

### 3. 백업된 알람 데이터 및 스크립트 압축
```
  tar cvzf alarm_backup.tar.gz backup_alarm *.sh
```

## 데이터를 밀어 넣을 대상 PM 접속
### 1. 위에서 백업 후 압축한 스크립트 다운로드 및 압축해제
```
  tar xvzf alarm_backup.tar.gz
```

### 2. 밀어 넣기 스크립트 실행
```
  ./restore_alarm.sh
```

# 백업 대상 테이블
```
T_AlarmBackup_IP_% 
T_Alarm_IP 
T_AlarmBackup_PC_% 
T_Alarm_PC 
T_AlarmBackup_System_% 
T_Alarm_System 
T_AlarmBackup_WDI_% 
T_Alarm_WDI 
T_UserEventBackup_% 
T_UserEvent 
T_UA_UserEventBackup_% 
T_UA_UserEvent 
T_UA_UserEventNicBackup_% 
T_UA_UserEvent_NICInfo 
T_AlarmBackup_HA_% 
T_Alarm_HA 
T_AlarmBackup_IDS_% 
T_Alarm_IDS 
T_AlarmBackup_Traffic_% 
T_Alarm_Traffic 
T_AlarmBackupInfo 
```