#!/bin/bash

mysql_cmd="/usr/local/mysql/bin/mysql -uroot -pipplus -DDBN_IPPlus -e"

start_check_monitoring_query="""
UPDATE T_ModuleTimestamp
SET CheckFlag=1, iTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN iTimestamp ELSE GetTimeTFromDateTime(NOW()) END, strTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN strTimestamp ELSE NOW() END
WHERE CheckFlag=2
"""

stop_check_monitoring_query="""
UPDATE T_ModuleTimestamp
SET CheckFlag=2, iTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN iTimestamp ELSE GetTimeTFromDateTime(NOW()) END, strTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN strTimestamp ELSE NOW() END
WHERE CheckFlag=1
"""

# stop_check_monitoring_query
echo "stop_check_monitoring_query"
echo "${mysql_cmd} ${stop_check_monitoring_query}"
${mysql_cmd} "${stop_check_monitoring_query}" 2> /dev/null
echo ""

# restore alarm
dirname_backup_alarm='backup_alarm'
#year=$(date +'%Y')
#year=$(( ${year} - 10 ))
year="1989"
restor_date="${year}$(date +'%m%d')"
echo restore_date : ${restor_date}

for table_name in $(ls ${dirname_backup_alarm}) ; do
	echo "/usr/local/mysql/bin/mysql -u root -pipplus -D DBN_IPPlus --default-character-set=latin1 < ${dirname_backup_alarm}/${table_name}"
	/usr/local/mysql/bin/mysql -u root -pipplus -D DBN_IPPlus --default-character-set=latin1 < ${dirname_backup_alarm}/${table_name}
done

# start_check_monitoring_query
echo ""
echo "start_check_monitoring_query"
echo "${mysql_cmd} ${start_check_monitoring_query}"
${mysql_cmd} "${start_check_monitoring_query}" 2> /dev/null
