#!/bin/bash

mysql_cmd="/usr/local/mysql/bin/mysql -uroot -pipplus -DDBN_IPPlus -e"
data_backup_query="""
select table_name from Information_schema.tables
where TABLE_SCHEMA = 'DBN_IPPlus'
  and TABLE_TYPE not like 'VIEW'
  and (
  table_name like '%T_AlarmBackup_IP_%'
  or table_name like '%T_AlarmBackup_PC_%'
  or table_name like '%T_AlarmBackup_System_%'
  or table_name like '%T_AlarmBackup_WDI_%'
  or table_name like '%T_UserEventBackup_%'
  or table_name like '%T_UA_UserEventBackup_%'
  or table_name like '%T_UA_UserEventNicBackup_%'
  or table_name like '%T_AlarmBackup_HA_%'
  or table_name like '%T_AlarmBackup_IDS_%'
  or table_name like '%T_AlarmBackup_Traffic_%'
  or table_name like 'T_Alarm_IP'
  or table_name like 'T_Alarm_PC'
  or table_name like 'T_Alarm_System'
  or table_name like 'T_Alarm_WDI'
  or table_name like 'T_UserEvent'
  or table_name like 'T_UA_UserEvent'
  or table_name like 'T_UA_UserEvent_NICInfo'
  or table_name like 'T_Alarm_HA'
  or table_name like 'T_Alarm_IDS'
  or table_name like 'T_Alarm_Traffic'
  or table_name like 'T_AlarmBackupInfo'
  )
"""
filename_backup_table_list="/tmp/alarm_backup_file_list.txt"
dirname_backup_data="./backup_alarm"
mkdir -p ${dirname_backup_data}

start_check_monitoring_query="""
UPDATE T_ModuleTimestamp
SET CheckFlag=1, iTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN iTimestamp ELSE GetTimeTFromDateTime(NOW()) END, strTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN strTimestamp ELSE NOW() END
WHERE CheckFlag=2
"""

stop_check_monitoring_query="""
UPDATE T_ModuleTimestamp
SET CheckFlag=2, iTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN iTimestamp ELSE GetTimeTFromDateTime(NOW()) END, strTimestamp=CASE WHEN iAlarmSentTimestamp>0 THEN strTimestamp ELSE NOW() END
WHERE CheckFlag=1
"""

# stop_check_monitoring_query
echo "stop_check_monitoring_query"
echo "${mysql_cmd} ${stop_check_monitoring_query}"
${mysql_cmd} "${stop_check_monitoring_query}" 2> /dev/null
echo ""

# save backup table list
echo "${mysql_cmd} ${data_backup_query} > ${filename_backup_table_list}"
${mysql_cmd} "${data_backup_query}" > ${filename_backup_table_list}

while read table_name ; do
	mysqlbackup_cmd="/usr/local/mysql/bin/mysqldump -u root -pipplus DBN_IPPlus ${table_name} --set-charset --default-character-set=latin1 --lock-table=0 --extended-insert=FALSE --force --complete-insert --insert-ignore"
	echo "${mysqlbackup_cmd} > ${dirname_backup_data}/${table_name}.sql"
	${mysqlbackup_cmd} > ${dirname_backup_data}/${table_name}.sql
done < ${filename_backup_table_list}

# start_check_monitoring_query
echo ""
echo "start_check_monitoring_query"
echo "${mysql_cmd} ${start_check_monitoring_query}"
${mysql_cmd} "${start_check_monitoring_query}" 2> /dev/null
